

import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, ref, set, onChildAdded, onChildRemoved, remove, update, onChildChanged } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyDRvorY4EO_33-oCsUD1xW8ZgYlE2y1tTA",
    authDomain: "proyectofinal-4a185.firebaseapp.com",
    projectId: "proyectofinal-4a185",
    storageBucket: "proyectofinal-4a185.appspot.com",
    messagingSenderId: "602820374520",
    appId: "1:602820374520:web:f628ee5c7b480cfdf3700d"
};
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);


function uploadImage(file, callback) {
    const sRef = storageRef(storage, 'imagen/' + Date.now() + '-' + file.name);
    const uploadTask = uploadBytesResumable(sRef, file);
    const carga = document.querySelector('.spinner');
    carga.classList.add('spiner2');

    uploadTask.on('state_changed',
        (snapshot) => {
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
        },
        (error) => {
            console.error('Error uploading image:', error);
            carga.classList.remove('spiner2');
        },
        () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                callback(downloadURL);
                carga.classList.remove('spiner2');
            });
        }
    );
}
function resetForm() {
    document.getElementById('titulo').value = '';
    document.getElementById('precio').value = '';
    document.getElementById('descripcion').value = '';
    document.getElementById('cantidad').value = '';
    document.getElementById('bookImage').value = '';
}

document.getElementById('bookForm').addEventListener('submit', function (event) {
    event.preventDefault();

    const categoria = document.getElementById('categoria').value;
    const titulo = document.getElementById('titulo').value;
    const precio = document.getElementById('precio').value;
    const descripcion = document.getElementById('descripcion').value;
    const cantidad = document.getElementById('cantidad').value;
    const imageFile = document.getElementById('bookImage').files[0];
    const existingKey = document.getElementById('bookId').value;

    saveBook(categoria, titulo, precio, descripcion, cantidad, imageFile, existingKey);

    resetForm();  // Aquí limpiamos el formulario
});

function saveBook(categoria, titulo, precio, descripcion, cantidad, imageFile, existingKey) {
    if (!titulo || !precio || !descripcion || !cantidad || !categoria) {
        console.error("Datos incompletos, no se guardará el libro");
        return;
    }
    function storeData(imageUrl) {
        const data = {
            titulo: titulo,
            precio: precio,
            descripcion: descripcion,
            cantidad: cantidad,
            imageUrl: imageUrl
        };
        if (existingKey) {
            const bookRef = ref(db, `libros/${categoria}/` + existingKey);
            update(bookRef, data);
        } else {
            const newBookRef = ref(db, `libros/${categoria}/` + Date.now());
            set(newBookRef, data);
        }
    }

    if (imageFile) {
        uploadImage(imageFile, (imageUrl) => {
            storeData(imageUrl);
        });
    } else {
        storeData(null);
    }
}


document.getElementById('bookForm').addEventListener('submit', function (event) {
    event.preventDefault();

    const titulo = document.getElementById('titulo').value;
    const precio = document.getElementById('precio').value;
    const descripcion = document.getElementById('descripcion').value;
    const cantidad = document.getElementById('cantidad').value;
    const imageFile = document.getElementById('bookImage').files[0];
    const existingKey = document.getElementById('bookId').value;

    saveBook(titulo, precio, descripcion, cantidad, imageFile, existingKey);

    // Limpiar el formulario después de guardar el libro
    event.target.reset();
});

document.getElementById('editBookForm').addEventListener('submit', function (event) {
    event.preventDefault();

    const editCategoria = document.getElementById('editCategoria').value; // <- Nuevo
    const titulo = document.getElementById('editTitulo').value;
    const precio = document.getElementById('editPrecio').value;
    const descripcion = document.getElementById('editDescripcion').value;
    const cantidad = document.getElementById('editCantidad').value;
    const imageFile = document.getElementById('editBookImage').files[0];
    const existingKey = document.getElementById('editBookId').value;

    saveBook(editCategoria, titulo, precio, descripcion, cantidad, imageFile, existingKey); // <- Pasa la categoría aquí

    cancelEdit();
});


function deleteBook(categoria, bookId) {
    const bookRef = ref(db, `libros/${categoria}/` + bookId);
    remove(bookRef);
}

document.addEventListener('DOMContentLoaded', (event) => {
    const cancelButton = document.querySelector('[data-cancel-button]');
    if (cancelButton) {
        cancelButton.addEventListener('click', cancelEdit);
    }
});

function cancelEdit() {
    document.getElementById('editBookForm').style.display = 'none';

    document.getElementById('editTitulo').value = '';
    document.getElementById('editPrecio').value = '';
    document.getElementById('editDescripcion').value = '';
    document.getElementById('editCantidad').value = '';
    document.getElementById('editBookId').value = '';
}
function editBook(categoria, bookId, data) {
    document.getElementById('editBookForm').style.display = 'block';

    document.getElementById('editCategoria').value = categoria; // <- Nuevo
    document.getElementById('editTitulo').value = data.titulo;
    document.getElementById('editPrecio').value = data.precio;
    document.getElementById('editDescripcion').value = data.descripcion;
    document.getElementById('editCantidad').value = data.cantidad;
    document.getElementById('editBookId').value = bookId;
}




function updateBookInView(bookId, data) {
    const li = document.getElementById(bookId);

    if (li) {
        // Actualizar el texto del libro
        li.childNodes[0].nodeValue = `${data.titulo} - ${data.precio} - ${data.descripcion} - ${data.cantidad}`;

        // Actualizar la imagen del libro
        const img = li.querySelector("img");
        img.src = data.imageUrl;
        img.alt = data.titulo;
    }
}



function displayBooks() {
    const categorias = ["ciencia_ficcion", "fantasia", "historia", "bestseller", "terror", "drama"];

    // Crear tabla y añadirla al DOM
    const table = document.createElement("table");
    const thead = document.createElement("thead");
    const tbody = document.createElement("tbody");
    table.appendChild(thead);
    table.appendChild(tbody);
    table.classList.add('table')
    table.classList.add('table-dark');
    document.getElementById('bookList').appendChild(table);

    // Crear encabezado de la tabla
    const trHeader = document.createElement("tr");
    ["Categoría", "Título", "Precio", "Descripción", "Cantidad", "Imagen", "Acciones"].forEach(headerText => {
        const th = document.createElement("th");
        th.textContent = headerText;
        trHeader.appendChild(th);
    });
    thead.appendChild(trHeader);

    categorias.forEach(categoria => {
        const booksRef = ref(db, 'libros/' + categoria);

        // Añadir libros
        onChildAdded(booksRef, snapshot => {
            console.log(`Añadiendo libro con ID ${snapshot.key} en la categoría ${categoria}`);

            const data = snapshot.val();
            const tr = document.createElement("tr");
            tr.id = `${categoria}-${snapshot.key}`;  // Aseguramos un ID único

            // Categoría
            const tdCategory = document.createElement("td");
            tdCategory.textContent = categoria;
            tr.appendChild(tdCategory);

            // Título
            const tdTitle = document.createElement("td");
            tdTitle.textContent = data.titulo;
            tr.appendChild(tdTitle);

            // Precio
            const tdPrice = document.createElement("td");
            tdPrice.textContent = data.precio;
            tr.appendChild(tdPrice);

            // Descripción
            const tdDescription = document.createElement("td");
            tdDescription.textContent = data.descripcion;
            tr.appendChild(tdDescription);

            // Cantidad
            const tdQuantity = document.createElement("td");
            tdQuantity.textContent = data.cantidad;
            tr.appendChild(tdQuantity);

            // Imagen
            const tdImage = document.createElement("td");
            const img = document.createElement("img");
            img.src = data.imageUrl;
            img.alt = data.titulo;
            img.width = 100;
            tdImage.appendChild(img);
            tr.appendChild(tdImage);

            // Acciones
            const tdActions = document.createElement("td");
            const deleteButton = document.createElement("button");
            deleteButton.classList.add('botonE')
            deleteButton.innerText = "Eliminar";
            deleteButton.onclick = () => deleteBook(categoria, snapshot.key);
            tdActions.appendChild(deleteButton);

            const editButton = document.createElement("button");
            editButton.classList.add('botonEdi');
            editButton.innerText = "Editar";
            editButton.onclick = () => editBook(categoria, snapshot.key, data); // <- Actualizado

            tdActions.appendChild(editButton);
            tr.appendChild(tdActions);

            tbody.appendChild(tr);
        });

        // Actualizar libros
        onChildChanged(booksRef, snapshot => {
            console.log(`Actualizando libro con ID ${snapshot.key}`);
            const data = snapshot.val();
            updateBookInTable(`${categoria}-${snapshot.key}`, data);
        });

        // Eliminar libros
        onChildRemoved(booksRef, (snapshot) => {
            const trToRemove = document.getElementById(`${categoria}-${snapshot.key}`);
            if (trToRemove) {
                trToRemove.remove();
            }
        });
    });
}


function updateBookInTable(bookId, data) {
    const tr = document.getElementById(bookId);
    if (tr) {
        tr.children[1].textContent = data.titulo;       // Título
        tr.children[2].textContent = data.precio;       // Precio
        tr.children[3].textContent = data.descripcion;  // Descripción
        tr.children[4].textContent = data.cantidad;     // Cantidad
        const img = tr.querySelector("img");            // Imagen
        img.src = data.imageUrl;
        img.alt = data.titulo;
    }
}

displayBooks();
function displayTodosBooks() {
    const categorias = ["ciencia_ficcion", "fantasia", "historia", "bestseller"];
    const container = document.getElementById('bookListTodos');

    categorias.forEach(categoria => {
        const booksRef = ref(db, 'libros/' + categoria);

        // Añadir libros
        onChildAdded(booksRef, snapshot => {
            console.log(`Añadiendo bestseller con ID ${snapshot.key}`);

            const data = snapshot.val();
            const div = document.createElement("div");
            div.classList.add('carta')
            div.id = `bestseller-${snapshot.key}`;
            // Crear y añadir la imagen
            const img = document.createElement("img");
            const divimg = document.createElement('div');
            img.src = data.imageUrl;
            img.alt = data.titulo;
            img.width = 100;
            img.classList.add('element-to-reveal');
            img.classList.add('img2');
            divimg.appendChild(img);
            divimg.classList.add('espacio_img');
            div.appendChild(divimg);
            // Crear y añadir el título del libro
            const div2 = document.createElement('div')
            div2.classList.add('espacio');
            const h1Title = document.createElement("h1");
            h1Title.textContent = data.titulo;
            h1Title.classList.add('h1-titulo');
            div2.appendChild(h1Title);
            div.appendChild(div2);

            // Crear y añadir el precio

            // Crear y añadir la descripción
            const div3 = document.createElement('div');
            div3.classList.add('espacio');
            const pDescription = document.createElement("p");
            pDescription.textContent = data.descripcion;
            pDescription.classList.add('hiden');
            div3.appendChild(pDescription);
            div.appendChild(div3);

            // Crear y añadir la cantidad disponible
            // const pQuantity = document.createElement("p");
            // pQuantity.textContent = `${data.cantidad}`;
            // div.appendChild(pQuantity);

            const pPrice = document.createElement("p");
            pPrice.textContent = `$${data.precio}`;
            pPrice.classList.add('precio')
            div.appendChild(pPrice);



            // Añadir botones de acciones si son necesarios aquí


            const tdActions = document.createElement("div");
            const deleteButton = document.createElement("button");
            deleteButton.classList.add('botonE')
            deleteButton.innerText = "Eliminar";
            deleteButton.onclick = () => deleteBook(categoria, snapshot.key);
            tdActions.appendChild(deleteButton);

            const editButton = document.createElement("button");
            editButton.classList.add('botonEdi');
            editButton.innerText = "Editar";
            editButton.onclick = () => editBook(categoria, snapshot.key, data); // <- Actualizado

            tdActions.appendChild(editButton);
            div.appendChild(tdActions);







            // Añadir el div completo al contenedor
            const a = document.createElement('a');
            a.classList.add('a');
            a.href = "#"
            a.appendChild(div)
            container.appendChild(a);
        });
        // Actualizar libros
        onChildChanged(booksRef, snapshot => {
            console.log(`Actualizando libro con ID ${snapshot.key}`);
            const data = snapshot.val();
            updateBookInTable(`${categoria}-${snapshot.key}`, data);
        });

        // Eliminar libros
        onChildRemoved(booksRef, (snapshot) => {
            const trToRemove = document.getElementById(`${categoria}-${snapshot.key}`);
            if (trToRemove) {
                trToRemove.remove();
            }
        });
    });
}
//llamanda a todas las categorias
displayTodosBooks();
