import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, ref, set, onChildAdded, onChildRemoved, remove, update } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';

const firebaseConfig = {
    apiKey: "AIzaSyDRvorY4EO_33-oCsUD1xW8ZgYlE2y1tTA",
    authDomain: "proyectofinal-4a185.firebaseapp.com",
    projectId: "proyectofinal-4a185",
    storageBucket: "proyectofinal-4a185.appspot.com",
    messagingSenderId: "602820374520",
    appId: "1:602820374520:web:f628ee5c7b480cfdf3700d"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);


//Con este funcion puedo mandar a llamar a todas las categorias en general
function displayBooks() {
    const categorias = ["ciencia_ficcion", "fantasia", "historia", "bestseller"];
    const container = document.getElementById('bookList');

    categorias.forEach(categoria => {
        const booksRef = ref(db, 'libros/' + categoria);

        // Añadir libros
        onChildAdded(booksRef, snapshot => {
            console.log(`Añadiendo bestseller con ID ${snapshot.key}`);

            const data = snapshot.val();
            const div = document.createElement("div");
            div.classList.add('carta')
            div.id = `bestseller-${snapshot.key}`;
            // Crear y añadir la imagen
            const img = document.createElement("img");
            const divimg = document.createElement('div');
            img.src = data.imageUrl;
            img.alt = data.titulo;
            img.width = 100;
            img.classList.add('element-to-reveal');
            img.classList.add('img');
            divimg.appendChild(img);
            divimg.classList.add('espacio_img');
            div.appendChild(divimg);
            // Crear y añadir el título del libro
            const div2 = document.createElement('div')
            div2.classList.add('espacio');
            const h1Title = document.createElement("h1");
            h1Title.textContent = data.titulo;
            h1Title.classList.add('h1-titulo');
            div2.appendChild(h1Title);
            div.appendChild(div2);

            // Crear y añadir el precio

            // Crear y añadir la descripción
            const div3 = document.createElement('div');
            div3.classList.add('espacio');
            const pDescription = document.createElement("p");
            pDescription.textContent = data.descripcion;
            pDescription.classList.add('hiden');
            div3.appendChild(pDescription);
            div.appendChild(div3);

            // Crear y añadir la cantidad disponible
            // const pQuantity = document.createElement("p");
            // pQuantity.textContent = `${data.cantidad}`;
            // div.appendChild(pQuantity);

            const pPrice = document.createElement("p");
            pPrice.textContent = `$${data.precio}`;
            pPrice.classList.add('precio')
            div.appendChild(pPrice);



            // Añadir botones de acciones si son necesarios aquí

            // Añadir el div completo al contenedor
            const a = document.createElement('a');
            a.classList.add('a');
            a.href = "#"
            a.appendChild(div)
            container.appendChild(a);
        });
    });
}
//llamanda a todas las categorias
displayBooks();

//Con esta funcion mando a llamar a solo a los bestseller
function displayBestsellers() {
    const categoria = "bestseller";

    // Referencia al contenedor donde se añadirán los libros
    const container = document.getElementById('bookList1');

    // Referencia a los libros de la categoría bestseller en la base de datos
    const booksRef = ref(db, 'libros/' + categoria);

    // Añadir libros bestseller
    onChildAdded(booksRef, snapshot => {
        console.log(`Añadiendo bestseller con ID ${snapshot.key}`);

        const data = snapshot.val();
        const div = document.createElement("div");
        div.classList.add('carta')
        div.id = `bestseller-${snapshot.key}`;
        // Crear y añadir la imagen
        const img = document.createElement("img");
        const divimg = document.createElement('div');
        img.src = data.imageUrl;
        img.alt = data.titulo;
        img.width = 100;
        img.classList.add('element-to-reveal');
        img.classList.add('img');
        divimg.appendChild(img);
        divimg.classList.add('espacio_img');
        div.appendChild(divimg);
        // Crear y añadir el título del libro
        const div2 = document.createElement('div')
        div2.classList.add('espacio');
        const h1Title = document.createElement("h1");
        h1Title.textContent = data.titulo;
        h1Title.classList.add('h1-titulo');
        div2.appendChild(h1Title);
        div.appendChild(div2);

        // Crear y añadir el precio

        // Crear y añadir la descripción
        const div3 = document.createElement('div');
        div3.classList.add('espacio');
        const pDescription = document.createElement("p");
        pDescription.textContent = data.descripcion;
        pDescription.classList.add('hiden');
        div3.appendChild(pDescription);
        div.appendChild(div3);

        // Crear y añadir la cantidad disponible
        // const pQuantity = document.createElement("p");
        // pQuantity.textContent = `${data.cantidad}`;
        // div.appendChild(pQuantity);

        const pPrice = document.createElement("p");
        pPrice.textContent = `$${data.precio}`;
        pPrice.classList.add('precio')
        div.appendChild(pPrice);



        // Añadir botones de acciones si son necesarios aquí

        // Añadir el div completo al contenedor
        const a = document.createElement('a');
        a.classList.add('a');
        a.href = "#"
        a.appendChild(div)
        container.appendChild(a);
    });


}

// Llamar a la función para mostrar los bestsellers
displayBestsellers();

/**Drama */
function displayDrama() {
    const categoria = "drama";

    // Referencia al contenedor donde se añadirán los libros
    const container = document.getElementById('bookList2');

    // Referencia a los libros de la categoría bestseller en la base de datos
    const booksRef = ref(db, 'libros/' + categoria);

    // Añadir libros bestseller
    onChildAdded(booksRef, snapshot => {
        console.log(`Añadiendo bestseller con ID ${snapshot.key}`);

        const data = snapshot.val();
        const div = document.createElement("div");
        div.classList.add('carta')
        div.id = `bestseller-${snapshot.key}`;
        // Crear y añadir la imagen
        const img = document.createElement("img");
        const divimg = document.createElement('div');
        img.src = data.imageUrl;
        img.alt = data.titulo;
        img.width = 100;
        img.classList.add('element-to-reveal');
        img.classList.add('img');
        divimg.appendChild(img);
        divimg.classList.add('espacio_img');
        div.appendChild(divimg);
        // Crear y añadir el título del libro
        const div2 = document.createElement('div')
        div2.classList.add('espacio');
        const h1Title = document.createElement("h1");
        h1Title.textContent = data.titulo;
        h1Title.classList.add('h1-titulo');
        div2.appendChild(h1Title);
        div.appendChild(div2);

        // Crear y añadir el precio

        // Crear y añadir la descripción
        const div3 = document.createElement('div');
        div3.classList.add('espacio');
        const pDescription = document.createElement("p");
        pDescription.textContent = data.descripcion;
        pDescription.classList.add('hiden');
        div3.appendChild(pDescription);
        div.appendChild(div3);

        // Crear y añadir la cantidad disponible
        // const pQuantity = document.createElement("p");
        // pQuantity.textContent = `${data.cantidad}`;
        // div.appendChild(pQuantity);

        const pPrice = document.createElement("p");
        pPrice.textContent = `$${data.precio}`;
        pPrice.classList.add('precio')
        div.appendChild(pPrice);



        // Añadir botones de acciones si son necesarios aquí

        // Añadir el div completo al contenedor
        const a = document.createElement('a');
        a.classList.add('a');
        a.href = "#"
        a.appendChild(div)
        container.appendChild(a);
    });


}

// Llamar a la función para mostrar los bestsellers
displayDrama();
/**Ciencia  */
function displayCiencia() {
    const categoria = "ciencia_ficcion";

    // Referencia al contenedor donde se añadirán los libros
    const container = document.getElementById('bookList3');

    // Referencia a los libros de la categoría bestseller en la base de datos
    const booksRef = ref(db, 'libros/' + categoria);

    // Añadir libros bestseller
    onChildAdded(booksRef, snapshot => {
        console.log(`Añadiendo bestseller con ID ${snapshot.key}`);

        const data = snapshot.val();
        const div = document.createElement("div");
        div.classList.add('carta')
        div.id = `bestseller-${snapshot.key}`;
        // Crear y añadir la imagen
        const img = document.createElement("img");
        const divimg = document.createElement('div');
        img.src = data.imageUrl;
        img.alt = data.titulo;
        img.width = 100;
        img.classList.add('element-to-reveal');
        img.classList.add('img');
        divimg.appendChild(img);
        divimg.classList.add('espacio_img');
        div.appendChild(divimg);
        // Crear y añadir el título del libro
        const div2 = document.createElement('div')
        div2.classList.add('espacio');
        const h1Title = document.createElement("h1");
        h1Title.textContent = data.titulo;
        h1Title.classList.add('h1-titulo');
        div2.appendChild(h1Title);
        div.appendChild(div2);

        // Crear y añadir el precio

        // Crear y añadir la descripción
        const div3 = document.createElement('div');
        div3.classList.add('espacio');
        const pDescription = document.createElement("p");
        pDescription.textContent = data.descripcion;
        pDescription.classList.add('hiden');
        div3.appendChild(pDescription);
        div.appendChild(div3);

        // Crear y añadir la cantidad disponible
        // const pQuantity = document.createElement("p");
        // pQuantity.textContent = `${data.cantidad}`;
        // div.appendChild(pQuantity);

        const pPrice = document.createElement("p");
        pPrice.textContent = `$${data.precio}`;
        pPrice.classList.add('precio')
        div.appendChild(pPrice);



        // Añadir botones de acciones si son necesarios aquí

        // Añadir el div completo al contenedor
        const a = document.createElement('a');
        a.classList.add('a');
        a.href = "#"
        a.appendChild(div)
        container.appendChild(a);
    });


}

// Llamar a la función para mostrar los bestsellers
displayCiencia();
/**Fantasia */

/**Ciencia  */
function displayFantasia() {
    const categoria = "fantasia";

    // Referencia al contenedor donde se añadirán los libros
    const container = document.getElementById('bookList4');

    // Referencia a los libros de la categoría bestseller en la base de datos
    const booksRef = ref(db, 'libros/' + categoria);

    // Añadir libros bestseller
    onChildAdded(booksRef, snapshot => {
        console.log(`Añadiendo bestseller con ID ${snapshot.key}`);

        const data = snapshot.val();
        const div = document.createElement("div");
        div.classList.add('carta')
        div.id = `bestseller-${snapshot.key}`;
        // Crear y añadir la imagen
        const img = document.createElement("img");
        const divimg = document.createElement('div');
        img.src = data.imageUrl;
        img.alt = data.titulo;
        img.width = 100;
        img.classList.add('element-to-reveal');
        img.classList.add('img');
        divimg.appendChild(img);
        divimg.classList.add('espacio_img');
        div.appendChild(divimg);
        // Crear y añadir el título del libro
        const div2 = document.createElement('div')
        div2.classList.add('espacio');
        const h1Title = document.createElement("h1");
        h1Title.textContent = data.titulo;
        h1Title.classList.add('h1-titulo');
        div2.appendChild(h1Title);
        div.appendChild(div2);

        // Crear y añadir el precio

        // Crear y añadir la descripción
        const div3 = document.createElement('div');
        div3.classList.add('espacio');
        const pDescription = document.createElement("p");
        pDescription.textContent = data.descripcion;
        pDescription.classList.add('hiden');
        div3.appendChild(pDescription);
        div.appendChild(div3);

        // Crear y añadir la cantidad disponible
        // const pQuantity = document.createElement("p");
        // pQuantity.textContent = `${data.cantidad}`;
        // div.appendChild(pQuantity);

        const pPrice = document.createElement("p");
        pPrice.textContent = `$${data.precio}`;
        pPrice.classList.add('precio')
        div.appendChild(pPrice);



        // Añadir botones de acciones si son necesarios aquí

        // Añadir el div completo al contenedor
        const a = document.createElement('a');
        a.classList.add('a');
        a.href = "#"
        a.appendChild(div)
        container.appendChild(a);
    });


}

// Llamar a la función para mostrar los bestsellers
displayFantasia();
/**Historia */
function displayHistoria() {
    const categoria = "historia";

    // Referencia al contenedor donde se añadirán los libros
    const container = document.getElementById('bookList5');

    // Referencia a los libros de la categoría bestseller en la base de datos
    const booksRef = ref(db, 'libros/' + categoria);

    // Añadir libros bestseller
    onChildAdded(booksRef, snapshot => {
        console.log(`Añadiendo bestseller con ID ${snapshot.key}`);

        const data = snapshot.val();
        const div = document.createElement("div");
        div.classList.add('carta')
        div.id = `bestseller-${snapshot.key}`;
        // Crear y añadir la imagen
        const img = document.createElement("img");
        const divimg = document.createElement('div');
        img.src = data.imageUrl;
        img.alt = data.titulo;
        img.width = 100;
        img.classList.add('element-to-reveal');
        img.classList.add('img');
        divimg.appendChild(img);
        divimg.classList.add('espacio_img');
        div.appendChild(divimg);
        // Crear y añadir el título del libro
        const div2 = document.createElement('div')
        div2.classList.add('espacio');
        const h1Title = document.createElement("h1");
        h1Title.textContent = data.titulo;
        h1Title.classList.add('h1-titulo');
        div2.appendChild(h1Title);
        div.appendChild(div2);

        // Crear y añadir el precio

        // Crear y añadir la descripción
        const div3 = document.createElement('div');
        div3.classList.add('espacio');
        const pDescription = document.createElement("p");
        pDescription.textContent = data.descripcion;
        pDescription.classList.add('hiden');
        div3.appendChild(pDescription);
        div.appendChild(div3);

        // Crear y añadir la cantidad disponible
        // const pQuantity = document.createElement("p");
        // pQuantity.textContent = `${data.cantidad}`;
        // div.appendChild(pQuantity);

        const pPrice = document.createElement("p");
        pPrice.textContent = `$${data.precio}`;
        pPrice.classList.add('precio')
        div.appendChild(pPrice);



        // Añadir botones de acciones si son necesarios aquí

        // Añadir el div completo al contenedor
        const a = document.createElement('a');
        a.classList.add('a');
        a.href = "#"
        a.appendChild(div)
        container.appendChild(a);
    });


}

// Llamar a la función para mostrar los bestsellers
displayHistoria();
/**terror */
function displayTerror() {
    const categoria = "terror";

    // Referencia al contenedor donde se añadirán los libros
    const container = document.getElementById('bookList6');

    // Referencia a los libros de la categoría bestseller en la base de datos
    const booksRef = ref(db, 'libros/' + categoria);

    // Añadir libros bestseller
    onChildAdded(booksRef, snapshot => {
        console.log(`Añadiendo bestseller con ID ${snapshot.key}`);

        const data = snapshot.val();
        const div = document.createElement("div");
        div.classList.add('carta')
        div.id = `bestseller-${snapshot.key}`;
        // Crear y añadir la imagen
        const img = document.createElement("img");
        const divimg = document.createElement('div');
        img.src = data.imageUrl;
        img.alt = data.titulo;
        img.width = 100;
        img.classList.add('element-to-reveal');
        img.classList.add('img');
        divimg.appendChild(img);
        divimg.classList.add('espacio_img');
        div.appendChild(divimg);
        // Crear y añadir el título del libro
        const div2 = document.createElement('div')
        div2.classList.add('espacio');
        const h1Title = document.createElement("h1");
        h1Title.textContent = data.titulo;
        h1Title.classList.add('h1-titulo');
        div2.appendChild(h1Title);
        div.appendChild(div2);

        // Crear y añadir el precio

        // Crear y añadir la descripción
        const div3 = document.createElement('div');
        div3.classList.add('espacio');
        const pDescription = document.createElement("p");
        pDescription.textContent = data.descripcion;
        pDescription.classList.add('hiden');
        div3.appendChild(pDescription);
        div.appendChild(div3);

        // Crear y añadir la cantidad disponible
        // const pQuantity = document.createElement("p");
        // pQuantity.textContent = `${data.cantidad}`;
        // div.appendChild(pQuantity);

        const pPrice = document.createElement("p");
        pPrice.textContent = `$${data.precio}`;
        pPrice.classList.add('precio')
        div.appendChild(pPrice);



        // Añadir botones de acciones si son necesarios aquí

        // Añadir el div completo al contenedor
        const a = document.createElement('a');
        a.classList.add('a');
        a.href = "#"
        a.appendChild(div)
        container.appendChild(a);
    });


}

// Llamar a la función para mostrar los bestsellers
displayTerror();
/**carrusel */
document.addEventListener('DOMContentLoaded', function () {
    const interiorCarrusel = document.querySelector('.interior-carrusel');
    const elementos = document.querySelectorAll('.elemento-carrusel');
    const botonAnterior = document.querySelector('.boton-carrusel.anterior');
    const botonSiguiente = document.querySelector('.boton-carrusel.siguiente');

    let indiceActual = 0;

    botonAnterior.addEventListener('click', moverAElementoAnterior);
    botonSiguiente.addEventListener('click', moverAElementoSiguiente);

    function actualizarBotones() {
        botonAnterior.style.display = indiceActual > 0 ? 'block' : 'none';
        botonSiguiente.style.display = indiceActual < elementos.length - 1 ? 'block' : 'none';
    }

    function moverAElementoSiguiente() {
        if (indiceActual < elementos.length - 1) {
            indiceActual++;
            moverCarruselA(indiceActual);
        }
    }

    function moverAElementoAnterior() {
        if (indiceActual > 0) {
            indiceActual--;
            moverCarruselA(indiceActual);
        }
    }

    function moverCarruselA(indice) {
        const ancho = interiorCarrusel.clientWidth;
        interiorCarrusel.style.transform = `translateX(-${indice * ancho}px)`;
        actualizarBotones();
    }

    actualizarBotones();
});

/* animacion elementos en la pantalla */
function revealElementsOnScroll() {
    const elements = document.querySelectorAll('.element-to-reveal');
    elements.forEach(element => {
        const elementTop = element.getBoundingClientRect().top;
        const windowBottom = window.innerHeight;

        if (elementTop < windowBottom) {
            element.classList.add('reveal');
        }
    });
}

// Ejecuta la función cuando se carga la página y cuando se desplaza.
window.addEventListener('load', revealElementsOnScroll);
window.addEventListener('scroll', revealElementsOnScroll);

/**carrusel */
